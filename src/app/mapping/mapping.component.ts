import { Component, OnInit } from "@angular/core";
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "tns-core-modules/ui/enums"; // used to describe at what accuracy the location should be get
import { PropertyChangeData } from "tns-core-modules/data/observable";
import { registerElement } from "nativescript-angular/element-registry";
import {
    MapboxViewApi,
    Viewport,
    Mapbox as MapboxViewport,
    MapboxCommon
} from "nativescript-mapbox";
registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);

@Component({
    selector: "ns-mapping",
    templateUrl: "./mapping.component.html",
    styleUrls: ["./mapping.component.css"],
    moduleId: module.id
})
export class MappingComponent implements OnInit {
    private map: MapboxViewApi;
    private mapbox: MapboxCommon;
    latitude;
    longitude;
    following: boolean = true;
    constructor() {}

    ngOnInit() {}

    // latitude="-26.1089262"
    // longitude="28.0536702"

    onMapReady(args) {
        this.map = args.map;

        geolocation.enableLocationRequest();
        // Get current location with high accuracy
        geolocation
            .getCurrentLocation({
                desiredAccuracy: Accuracy.high,
                maximumAge: 5000,
                timeout: 20000
            })
            .then(geolocation => {
                this.map.setCenter({
                    lat: geolocation.latitude,
                    lng: geolocation.longitude
                });

                this.map.getUserLocation().then(() => {
                    this.map.trackUser({
                        mode: "FOLLOW_WITH_COURSE",
                        animated: true
                    });
                });

                // this.mapbox.requestFineLocationPermission()

                this.map.addMarkers([
                    {
                        id: 1,
                        lat: geolocation.latitude,
                        lng: geolocation.longitude,
                        title: "You are here",
                        subtitle: "Defria Manda",
                        onTap: () => {
                            console.log("Ambulance on duty tapped");
                        },
                        onCalloutTap: () => {
                            console.log("Ambulance is on callout tapped");
                        }
                    },
                    {
                        id: 1,
                        lat: geolocation.latitude - 0.001,
                        lng: geolocation.longitude,
                        title: "Ambulance 102",
                        subtitle: "Ambulance dispatched",
                        onTap: () => {
                            console.log("Ambulance on dispatch tapped");
                        },
                        onCalloutTap: () => {
                            console.log(
                                "Ambulance is on dispatched callout tapped"
                            );
                        }
                    }
                ]);

                console.log(
                    "My location",
                    geolocation.latitude,
                    geolocation.longitude
                );
            });
    }

    toggleFollowing(args: PropertyChangeData): void {
        if (args.value !== null && args.value !== this.following) {
          this.following = args.value;
          // adding a timeout so the switch has time to animate properly
          setTimeout(() => {
            this.map.trackUser({
              mode: this.following ? "FOLLOW_WITH_COURSE" : "NONE",
              animated: true
            });
          }, 200);
        }
      }
    
}
