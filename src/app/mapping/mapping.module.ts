import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MappingRoutingModule } from "./mapping-routing.module"
import { MappingComponent } from "./mapping.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        MappingRoutingModule
    ],
    declarations: [
        MappingComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MapModule { }
